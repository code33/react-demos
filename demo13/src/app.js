import React from 'react';

export default class App extends React.Component{

  constructor(props) {
    //  props:props是一个组件的设置参数，可以在父控件中选择性设置。父组件对子控件的props进行赋值，并且props的值不可改变。一个子控件自身不能改变自己的 props。
    // 父控件对子控件进行传值用;
    super(props);
    this.render = this.render.bind(this);

    // state:当一个组件 mounts的时候，state如果设置有默认值的会被使用，并且state可能时刻的被改变。一个子控件自身可以管理自己的state，但是需要注意的是，无法管理其子控件的state。所以可以认为，state是子控件自身私有的。
    // 操作本控件自身数值
    this.state = {
      // 将从dom获取的数据转换至state中
      items: this.props.items,
      disabled: true
    };
  }

  componentDidMount() {
    // 加载完成后对 disable释放
    this.setState({
      disabled: false
    });
  }

  handleClick(ev) {

    var items = this.state.items
    
    items.push(('ItemA ' + this.state.items.length));
    
    this.setState({
      items: items
    })
  }

  render() {
    return (
      <div>
        <button onClick={(e)=>this.handleClick(e)} disabled={this.state.disabled}>Add Item</button>
        <ul>
        {
          this.state.items.map((item, index)=>{
            return <li key={index}>{item}</li>
          })
        }
        </ul>
      </div>
    )
  }
};
