# DEMO13

    This demo is copied from [github.com/mhart/react-server-example](https://github.com/mhart/react-server-example), but I rewrote it with JSX syntax.


## 操作说明

参考
babel 编译器教程
http://www.ruanyifeng.com/blog/2016/01/babel.html


```bash
# install the dependencies in demo13 directory
$ npm install

# translate all jsx file in src subdirectory to js file
$ npm run build

# launch http server
$ node server.js
```

调试命令
安装依赖包
```
yarn install 
```

编译发布
```
yarn build
```

运行
```
yarn start
```