import axios from 'axios'
export function setPageTitle (data) {
    return (dispatch, getState) => {
      dispatch({ type: 'SET_PAGE_TITLE', data: data })
    }
  }
  
export function setInfoList (data) {
    return (dispatch, getState) => {
      // 使用fetch实现异步请求
      axios.get('/api/getInfoList', {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json'
          }
      }).then((res,data) => {
          //console.log('setInfoList-->',res);
          //console.log(data);
          return res;  
      }).then((redata)=>{
          let { status, data } = redata
          console.log('res',redata)
          console.log('redata',data)
          console.log('status',status)
          if (status === 200) {
              dispatch({ type: 'SET_INFO_LIST', data: data })
          }
      })
    }
  } 