import React, { Component } from 'react';
// store 分离
import store from "./Store";
// action 分离
import * as Action from './Action'

export default class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      count: store.getState()
    }
  }

  onIncrement =()=>{
    store.dispatch(Action.increment())
  }

  onDecrement =()=>{
    store.dispatch(Action.decrement())
  }


  handleIncrement = (ev,inputN) => {
    console.log('input==>',inputN);
    this.setState({
      count: this.state.count + 1
    })
  }

  handleDecrement = () => {
    this.setState({
      count: this.state.count - 1
    })
  }

  render() {
    let that = this;
    //let ste=
    store.subscribe(() => {
      console.log("Store is changed: " + store.getState())
      that.setState({
        count: store.getState()
      })
      
    });
    return (
      <div className="container">
        <h1 className="text-center mt-5">{(this.state.count)}</h1>
        {/* <h1 className="text-center mt-5">{this.state.count}</h1> */}
        <p className="text-center">
          <button onClick={this.onIncrement} className="btn btn-primary mr-2">Increase</button>
          <button onClick={this.onDecrement} className="btn btn-danger my-2">Decrease</button>
        </p>
      </div>
    );
  }
}

 